import * as React from "react";
import styled from "styled-components";

const DragAndDrop = ({ className, onDrop }) => {
  const onChange = React.useCallback(event => {
    const { files } = event.target;
    const [file] = files;
    const reader = new FileReader();
    reader.onabort = () => console.log("file reading was aborted");
    reader.onerror = () => console.log("file reading has failed");
    reader.onload = () => {
      onDrop(JSON.parse(reader.result));
    };
    reader.readAsText(file);
  }, []);
  return (
    <div className={className}>
      <input onChange={onChange} type="file" />
    </div>
  );
};

const styledComponent = styled(DragAndDrop)`
  width: 300px;
  height: 300px;
  background-color: red;
`;

export default styledComponent;
