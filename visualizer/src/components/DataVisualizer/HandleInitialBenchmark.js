import * as React from "react";
import styled from "styled-components";

const HandleInitialBenchmark = ({ data }) => {
  const array_of_apps = Object.entries(data).map(([app_name, data]) =>
    getInitialBenchmarkData(app_name, data)
  );
  const parsed_data = array_of_apps.map(app => getX(app));
  return (
    <div>
      <Section>
        <h2>First paint time</h2>
        <Row>
          {parsed_data.map(data => (
            <Cell key={data.name}>
              <h4>{data.name}</h4>
              <p>{data.data.firstPaintTime} ms</p>
            </Cell>
          ))}
        </Row>
      </Section>
      <>
        {Object.values(ACTIONS).map(action => {
          return (
            <Section key={`${action}`}>
              <h2>{action}</h2>
              <Row>
                {parsed_data.map(data =>
                  getDataForBenchmarkAndAction(action, data)
                )}
              </Row>
            </Section>
          );
        })}
      </>
    </div>
  );
};

const Section = styled.section`

`

const Row = styled.div`
  display: flex;
  border: 1px solid;
  padding: 10px;
`;

const Cell = styled.div`
  margin-right: 1em;
`;

const getDataForBenchmarkAndAction = (action_name, data) => {
  const {
    name,
    data: { actions }
  } = data;
  const values = actions[action_name];

  return (
    <Cell key={`${action_name}/${name}`}>
      <h4>{name}</h4>
      <p>Min: {values.min} ms</p>
      <p>Max: {values.max} ms</p>
      <p>Averange: {values.avg} ms</p>
      <p>Median: {values.median} ms</p>
      <p>Standard deviation: {values.standard_deviation} ms</p>
    </Cell>
  );
};

const getInitialBenchmarkData = (name, data) => {
  return { name, data: data.benchmarks.single_action.iterations[0][0][1] };
};

const ACTIONS = {
  REPLACE_ALL: "replace_all_click",
  UPDATE_N_ROWS: "updateNRows_click",
  SWAP_TWO_ROWS: "swapTwoRows",
  REMOVE_ROW: "removeRow",
  APPEND_N_ROWS_BEGINNING: "appendNRowsBeginning_click",
  APPEND_N_ROWS_END: "appendNRowsEnd_click",
  CLEAR_ROWS: "clearRows_click"
};

const mapMeasurementsForAction = data =>
  Object.values(ACTIONS)
    .map(action => [
      action,
      data.measures.filter(item => item.name.indexOf(action) >= 0)
    ])
    .reduce((prev, [a, b]) => ({ ...prev, [a]: b }), {});

const makeMapFromArray = arr =>
  arr.reduce((prev, [key, value]) => ({ ...prev, [key]: value }), {});

const getX = ({ name, data }) => {
  const firstPaintTime = data.first_paint.startTime;
  const tmp = mapMeasurementsForAction(data);
  const analyzedActions = makeMapFromArray(
    Object.values(ACTIONS).map(action => [action, analyzeResult(tmp[action])])
  );
  return {
    name,
    data: {
      firstPaintTime,
      actions: analyzedActions
    }
  };
};

const sortFloats = arr =>
  arr.sort((a, b) => (parseFloat(a) > parseFloat(b) ? 1 : -1));

const get_median = arr => {
  const mid = Math.ceil(arr.length / 2);
  return arr.length % 2 == 0 ? (arr[mid] + arr[mid - 1]) / 2 : arr[mid];
};

// Arithmetic mean
let getMean = function (data) {
  return (
    data.reduce(function (a, b) {
      return Number(a) + Number(b);
    }) / data.length
  );
};

// Standard deviation
let getSD = function (data) {
  let m = getMean(data);
  return Math.sqrt(
    data.reduce(function (sq, n) {
      return sq + Math.pow(n - m, 2);
    }, 0) /
    (data.length - 1)
  );
};

const analyzeResult = data => {
  const pure_values = data.map(item => item.duration);
  const sorted_values = sortFloats(pure_values);
  const avg = getMean(pure_values);
  const max = pure_values.reduce((prev, next) => (prev > next ? prev : next));
  const min = pure_values.reduce((prev, next) => (prev < next ? prev : next));
  const median = get_median(sorted_values);
  const standard_deviation = getSD(pure_values);

  return {
    avg,
    min,
    max,
    median,
    pure_values,
    standard_deviation
  };
};

export default HandleInitialBenchmark;
