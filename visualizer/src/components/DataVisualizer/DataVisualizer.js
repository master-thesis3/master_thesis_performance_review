import * as React from "react";
import HandleInitialBenchmark from "./HandleInitialBenchmark";
import styled from "styled-components";

const DataVisualizer = ({ data }) => {
  const { applications } = data;

  return (
    <div>
      <Toggler title="Single action">
        <HandleInitialBenchmark data={applications} />
      </Toggler>
    </div>
  );
};

const Toggler = styled(({ className, children, title }) => {
  const [visible, setVisible] = React.useState(true);
  return (
    <div className={className}>
      <button onClick={() => setVisible(!visible)}>
        <h4>{title}</h4>
      </button>
      {visible && children}
    </div>
  );
})``;

export default DataVisualizer;
