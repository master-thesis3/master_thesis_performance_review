import * as React from "react";
import * as ReactDOM from "react-dom";
import styled from "styled-components";
import DragAndDrop from "./components/DragAndDrop/DragAndDrop";
import DataVisualizer from "./components/DataVisualizer";

class Main extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
  }

  onDataLoaded = data => {
    this.setState({
      data
    });
  };

  render() {
    const { className } = this.props;
    const { data } = this.state;
    return (
      <div className={`${className}`}>
        {!data && (
          <>
            <DragAndDrop onDrop={this.onDataLoaded} />
            <p>Drag 'n' drop some files here, or click to select files</p>
          </>
        )}
        {!!data && <DataVisualizer data={data} />}
      </div>
    );
  }
}

const StyledComponent = styled(Main)`
  font-family: "Raleway", sans-serif;
  margin: 10px;
`;

ReactDOM.render(<StyledComponent />, document.getElementById("app"));
