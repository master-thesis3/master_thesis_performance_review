import { Component, OnInit } from "@angular/core";
import { DataManipulatorService } from "./../data-manipulator.service";
import * as HelperBenchmark from "./helper_benchmark";

@Component({
  selector: "app-button-row",
  templateUrl: "./button-row.component.html",
  styleUrls: ["./button-row.component.css"]
})
export class ButtonRowComponent implements OnInit {
  constructor(private dataManipulatorService: DataManipulatorService) {}

  ngOnInit() {}

  onReplaceAllRowsClick(): void {
    this.dataManipulatorService.setData(
      HelperBenchmark.replaceAllRows(this.dataManipulatorService.getDataSync())
    );
  }

  onUpdateNRowsClick(): void {
    this.dataManipulatorService.setData(
      HelperBenchmark.updateNRows(250, this.dataManipulatorService.getDataSync())
    );
  }

  onSwapTwoRowsClick(): void {
    this.dataManipulatorService.setData(
      HelperBenchmark.swapRows(1, 2, this.dataManipulatorService.getDataSync())
    );
  }

  onRemoveRowClick(): void {
    this.dataManipulatorService.setData(
      HelperBenchmark.removeRow(1, this.dataManipulatorService.getDataSync())
    );
  }

  onAppendNRowsStartClick(): void {
    this.dataManipulatorService.setData(
      HelperBenchmark.appendNRowsStart(250, this.dataManipulatorService.getDataSync())
    );
  }

  onAppendNRowsEndClick(): void {
    this.dataManipulatorService.setData(
      HelperBenchmark.appendNRowsEnd(250, this.dataManipulatorService.getDataSync())
    );
  }

  onClearRowsClick(): void {
    this.dataManipulatorService.setData(
      HelperBenchmark.clearRows()
    );
  }
}
