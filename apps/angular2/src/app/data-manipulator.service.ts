import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataManipulatorService {

  data = []

  constructor() { }

  getData(): Observable<any[]> {
    return of(this.data)
  }

  getDataSync(): any[] {
    return this.data
  }

  setData(data: any[]): void {
    this.data.length = 0
    this.data.push(...data)
  }
}
