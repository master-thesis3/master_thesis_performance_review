import { TestBed } from '@angular/core/testing';

import { DataManipulatorService } from './data-manipulator.service';

describe('DataManipulatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataManipulatorService = TestBed.get(DataManipulatorService);
    expect(service).toBeTruthy();
  });
});
