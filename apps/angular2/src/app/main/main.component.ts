import { Component, OnInit } from '@angular/core';
import { DataManipulatorService } from './../data-manipulator.service'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  data: any[]

  constructor(private dataManipulatorService: DataManipulatorService) { }

  ngOnInit() {
    this.getData()
  }

  getData(): void {
    this.dataManipulatorService.getData().subscribe(data => this.data = data)
  }

}
