export const PROFILE = {
  CREATE_N_ROWS: "CREATE_N_ROWS",
  REPLACE_ALL_ROWS: "REPLACE_ALL_ROWS",
  UPDATE_N_ROWS: "UPDATE_N_ROWS",
  SWAP_TWO_ROWS: "SWAP_TWO_ROWS",
  REMOVE_ONE_ROW: "REMOVE_ONE_ROW",
  CLEAR_ROWS: "CLEAR_ROWS"
};

export const N = 1000; // base number of rows
const ALPHABET = "abcdefghijklmnopqrstuvwxyz0123456789".split("");

const getRandomNumber = (max = 1000000) =>
  (Math.random() * 1000000).toFixed(0) % max;
const getRandomChar = () => ALPHABET[getRandomNumber(ALPHABET.length)];

export const getRandomValue = () => {
  let val = "";
  for (let i = 0; i < 10; i++) {
    val += getRandomChar();
  }
  return val;
};

export const replaceAllRows = data => {
  return data.map(() => getRandomValue());
};

export const updateNRows = (rows_to_update, data) => {
  const newData = [...data];
  for (let index = 0; index < rows_to_update; index++) {
    const i = getRandomNumber(data.length);
    newData[i] = getRandomValue();
  }
  return newData;
};

export const swapRows = (indexA, indexB, data) => {
  const newData = [...data];
  let tmp = data[indexA];
  newData[indexA] = newData[indexB];
  newData[indexB] = tmp;
  return newData;
};

export const removeRow = (index, data) => {
  const newData = [...data];
  newData.splice(index, 1);
  return newData;
};

export const appendNRowsStart = (number_of_rows, data=[]) => {
  const newRows = [];
  for (let index = 0; index < number_of_rows; index++) {
    newRows.push(getRandomValue());
  }
  return [...newRows, ...data];
};

export const appendNRowsEnd = (number_of_rows, data=[]) => {
  const newRows = [];
  for (let index = 0; index < number_of_rows; index++) {
    newRows.push(getRandomValue());
  }
  return [...data, ...newRows];
};

export const clearRows = () => [];
