import * as React from "react";
import * as ReactDOM from "react-dom";
import {
  replaceAllRows,
  updateNRows,
  swapRows,
  removeRow,
  appendNRowsStart,
  appendNRowsEnd,
  clearRows
} from "./helper_benchmark";

const ButtonsRow = ({ state, onChange }) => {
  return (
    <div
      style={{
        position: "fixed",
        top: 0,
        right: 0
      }}
    >
      <button
        id="append-rows-start"
        onClick={() => onChange(appendNRowsStart(250, state))}
      >
         Append 250 rows start
      </button>
      <button
        id="append-rows-end"
        onClick={() => onChange(appendNRowsEnd(250, state))}
      >
        Append 250 rows end
      </button>
      <button
        id="replace-all-rows"
        onClick={() => onChange(replaceAllRows(state))}
      >
        replaceAllRows
      </button>
      <button
        id="update-n-rows"
        onClick={() => onChange(updateNRows(250, state))}
      >
        updateNRows
      </button>
      <button
        id="swap-two-rows"
        onClick={() => onChange(swapRows(1, 2, state))}
      >
        Swap Rows
      </button>
      <button id="remove-row" onClick={() => onChange(removeRow(1, state))}>
        Remove Row
      </button>
      <button id="clear-rows" onClick={() => onChange(clearRows())}>
        Clear rows
      </button>
    </div>
  );
};

const Li = ({ children }) => <li>{children}</li>

const Test = () => {
  const [data, setDate] = React.useState([]);

  return (
    <div>
      <ButtonsRow state={data} onChange={newState => setDate(newState)} />
      <ul>{data && data.map(item => <Li key={item}>{item}</Li>)}</ul>
    </div>
  );
};

ReactDOM.render(<Test />, document.getElementById("app"));
