FROM nginx:1.17.6-alpine
COPY ./releases /var/www
COPY nginx.conf /etc/nginx/conf.d/default.conf
LABEL maintainer="pufek93@gmail.com"
# Forward request logs to Docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
  && ln -sf /dev/stderr /var/log/nginx/error.log
EXPOSE 8080
STOPSIGNAL SIGTERM
CMD ["nginx","-g","daemon off;"]
