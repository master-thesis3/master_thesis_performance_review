const { LOCAL, PATH_TO_RESULTS } = process.env;

const DEFAULT_PATH_TO_RESULTS = "./results";

class Config {
  data = {
    isLocal: false,
    path_to_results: null
  };
  initialize() {
    this.data.isLocal = LOCAL || false;
    this.data.path_to_results = PATH_TO_RESULTS || DEFAULT_PATH_TO_RESULTS;
  }
}

module.exports = new Config();
