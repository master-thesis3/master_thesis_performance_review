const {
  handle_performance_logs,
  handle_browser_logs,
  handle_driver_logs
} = require("./handlers");
const { visualize_data } = require("./visualization");
const path = require("path");
const Config = require("./Config");
const {
  get_folder_list,
  read_file,
  sortIndexes,
  save_dist_file
} = require("./utils");

const LOG_TYPES = {
  PERFORMANCE: "performance",
  DRIVER: "driver",
  BROWSER: "browser"
};

(async () => {
  try {
    Config.initialize();
    const list_of_applications = await get_folder_list(
      Config.data.path_to_results
    );
    console.log("Discovered:", list_of_applications);
    const parsed_data = await handle_applications(list_of_applications);
    await save_dist_file(
      path.resolve(`./dist/index.json`),
      JSON.stringify(parsed_data, null, 2)
    );
    // await visualize_data(parsed_data);
  } catch (error) {
    console.error(`Error while analysing data: ${error}`);
    process.exitCode = 1;
  }
})();

const handle_applications = async list_of_applications => {
  const results = {
    applications: {}
  };
  for (const application of list_of_applications) {
    console.log("Starting analysis for:", application);
    const benchmark_result_folder = `${Config.data.path_to_results}/${application}`;
    const list_of_benchmarks = await get_folder_list(benchmark_result_folder);
    console.log(`Founded benchmarks: ${list_of_benchmarks}`);
    results.applications[application] = {
      benchmarks: {}
    };
    const benchmarks = await handle_benchmarks(benchmark_result_folder, list_of_benchmarks)
    benchmarks.forEach(([benchmark_name, iterations]) => {
      results.applications[application].benchmarks[benchmark_name] = {
        iterations
      }
    })
  }
  return results;
};

const handle_benchmarks = async (
  benchmark_result_folder,
  list_of_benchmarks,
) => {
  const results = [];
  for (const benchmark of list_of_benchmarks) {
    console.log("Starting analysis for:", benchmark);
    const benchmark_path = `${benchmark_result_folder}/${benchmark}`;
    const list_of_iterations = await get_folder_list(benchmark_path);
    const iterations_data = await handle_iterations(
      sortIndexes(list_of_iterations),
      benchmark_path
    );
    results.push([benchmark, iterations_data]);
  }
  return results;
};

const handle_iterations = async (list_of_iterations, benchmark_path) => {
  const result = [];
  for (const index of list_of_iterations) {
    const file_path = `${benchmark_path}/${index}`;
    const list_of_files = await get_folder_list(file_path);
    const data = await handle_files(file_path, list_of_files);
    result.push(data);
  }
  return result;
};

const handle_files = async (benchmark_path, list_of_files) => {
  const results = [];
  for (const file of list_of_files) {
    const file_path = `${benchmark_path}/${file}`;
    results.push(await handle_single_file(file_path));
  }
  return results;
};

const handle_single_file = async file_path => {
  console.log("Handle file:", file_path);
  try {
    const content = await read_file(file_path);
    const parsed_data = await JSON.parse(content);
    switch (true) {
      case /performance/.test(file_path):
        return [LOG_TYPES.PERFORMANCE, handle_performance_logs(parsed_data)];
      case /driver/.test(file_path):
        return [LOG_TYPES.DRIVER, handle_driver_logs(parsed_data)];
      case /browser/.test(file_path):
        return [LOG_TYPES.BROWSER, handle_browser_logs(parsed_data)];
      default:
        throw new Error("Missing proper handler");
    }
  } catch (error) {
    throw new Error(`Error while handling ${file_path}:\n${error}`);
  }
};
