const build_html = data => {
  return `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
     ${data.map(([application, ...benchmarks]) => {
       return `
        <h1>${application}</h1>
        ${parse_benchmarks(benchmarks)}
       `;
     })}   
    </body>
    </html>
    `;
};

const parse_benchmarks = benchmarks => {
  return benchmarks.map(([name, data]) => {
    return `
      <h2>${name}</h2>
      <table>
        <thead>
          <td>Index</td>
          <td>Data</td>
        </thead>
      <tbody>
        ${data.map((data, index) => {
          return `
            <tr>
              <td>${index}</td>
              <td>${data.map(x => parse_benchmark_data(x))}</td>
            </tr>
          `;
        })}
      </tbody>
    </table>
    
    `;
  });
};

const parse_benchmark_data = benchmark_data_array =>
  benchmark_data_array.map(data => {
    if (typeof data === "string") return "";
    if (data.length === 0) return "";
    return `
        <span>First paint ${data.first_paint.startTime}</span> </br>
    <h3>Measures</h3>
    ${data.measures.map(
      measure => `
        <span>${measure.name} ${measure.duration}</span> </br>
    `
    )}`;

    // return ``
  });

module.exports = {
  build_html
};
