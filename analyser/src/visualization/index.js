const { save_dist_file } = require("./../utils");
const { build_html } = require("./template");
const path = require("path");

const visualize_data = async data => {
  const html = await build_html(data);
  await save_dist_file(path.resolve(`./dist/index.html`), html);
};

module.exports = {
  visualize_data
};
