const INTERESTING_TYPES = ["mark", "measure", "paint"];

const extract_extra_info = filtered_data => {
  return {
    first_paint: filtered_data.find(item => item.name === "first-paint"),
    measures: filtered_data.filter(
      item => item.entryType === "measure" && item.name.indexOf("measure") >= 0
    )
  };
};

module.exports = data => {
  const messages = data
    .filter(item => item.level === "INFO")
    .filter(item => /console-api/.test(item.message))
    .map(item => item.message);
  const parsed = messages.map(item => item.substr(item.indexOf(`"{`)));
  const parsed_json = parsed.map(item => JSON.parse(JSON.parse(item)));
  const filtered_data = parsed_json.filter(item =>
    INTERESTING_TYPES.includes(item.entryType)
  );
  return extract_extra_info(filtered_data);
};
