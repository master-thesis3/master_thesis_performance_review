const handle_browser_logs = require('./handle_browser_logs')
const handle_driver_logs = require('./handle_driver_logs')
const handle_performance_logs = require('./handle_performance_logs')

module.exports = {
    handle_browser_logs,
    handle_driver_logs,
    handle_performance_logs,
}