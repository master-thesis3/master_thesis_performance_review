const fs = require("fs");
const path = require("path");

const get_folder_list = async path => {
  const list_of_applications = await fs.promises.readdir(path);
  return list_of_applications;
};

const read_file = async path => {
  return await fs.promises.readFile(path, "utf-8");
};

const save_dist_file = async (file_path, data) => {
  await fs.promises.mkdir(path.resolve(`./dist`), { recursive: true });
  return await fs.promises.writeFile(file_path, data);
};

const sortIndexes = list => list.sort((a,b) => parseInt(a) >= parseInt(b) ? 1 : -1)

module.exports = {
  get_folder_list,
  read_file,
  save_dist_file,
  sortIndexes,
};
