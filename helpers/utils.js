const { exec } = require("child_process");
const fs = require("fs");
const path = require("path");

const ALL_APPS_FOLDER = path.resolve("./apps");

const getDirectories = source =>
  fs
    .readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name);

const run_command = (app_path, command) =>
  new Promise((resolve, reject) => {
    exec(command, { cwd: app_path }, (error, stdout, stderr) => {
      if (error) return reject(error);
      resolve([stdout, stderr]);
    });
  });

module.exports = {
    ALL_APPS_FOLDER,
    getDirectories,
    run_command
}