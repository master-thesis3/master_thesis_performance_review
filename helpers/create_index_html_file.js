const { ALL_APPS_FOLDER, getDirectories, run_command } = require("./utils");
const path = require("path");
const fs = require("fs");

const TOP_LEVEL_RELEASE_FOLDER = path.resolve("./releases");

const return_template = apps => `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List of available apps</title>
</head>
<body>
  <h1>List of available apps:</h1>
  <ul id="list-of-links">
    ${apps.map(item => `<li>
      <a class="link" href="/${item}">${item}</a>
    </li>`).join('')}
  </ul>
</body>
</html>`;

const create_index_file = async (destination_path, template) => {
    const file_path = path.resolve(`${destination_path}/index.html`)
    await fs.promises.writeFile(file_path, template)
}

const main = async () => {
  const list_of_paths = getDirectories(ALL_APPS_FOLDER);

  if (!fs.existsSync(TOP_LEVEL_RELEASE_FOLDER)) {
    await fs.promises.mkdir(TOP_LEVEL_RELEASE_FOLDER);
  }

  const template = return_template(list_of_paths)
  create_index_file(TOP_LEVEL_RELEASE_FOLDER, template)
  console.log('Successfully created index.html file')

};

main();
