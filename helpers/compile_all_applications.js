const { ALL_APPS_FOLDER, getDirectories, run_command } = require("./utils");
const path = require("path");

const COMMANDS = {
  INSTALL: "make install",
  PRODUCTION: "make production"
};

const main = async () => {
  const list_of_paths = getDirectories(ALL_APPS_FOLDER);

  await Promise.all(
    list_of_paths.map(
      foler_name =>
        new Promise(async (resolve, reject) => {
          const resolved_path = path.resolve(ALL_APPS_FOLDER, foler_name);
          try {
            await run_command(resolved_path, COMMANDS.INSTALL);
            console.log("Success install", foler_name);
            await run_command(resolved_path, COMMANDS.PRODUCTION);
            console.log("Success creating production release of", foler_name);
            resolve();
          } catch (error) {
            console.error("Failed to prepare app", foler_name);
            reject();
          }
        })
    )
  );
};

main();
