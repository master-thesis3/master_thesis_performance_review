const { ALL_APPS_FOLDER, getDirectories, run_command } = require("./utils");
const path = require("path");
const fs = require("fs");

const TOP_LEVEL_RELEASE_FOLDER = path.resolve("./releases");
const RELEASE_FOLDER = "./dist";

const copy_dist = async (cwd, target, destination) => {
  await run_command(cwd, `cp -r ${target} ${destination}`);
};

const main = async () => {
  const list_of_paths = getDirectories(ALL_APPS_FOLDER);

  if (!fs.existsSync(TOP_LEVEL_RELEASE_FOLDER)) {
    await fs.promises.mkdir(TOP_LEVEL_RELEASE_FOLDER);
  }

  for (const foler_name of list_of_paths) {
    const resolved_folder_path = path.resolve(ALL_APPS_FOLDER, foler_name);
    const resolved_dist_path = path.resolve(
      resolved_folder_path,
      RELEASE_FOLDER
    );
    const destination_folder = path.resolve(
      `${TOP_LEVEL_RELEASE_FOLDER}/${foler_name}`
    );
    try {
      await copy_dist(
        resolved_folder_path,
        resolved_dist_path,
        destination_folder
      );
      console.log(`Copied ${foler_name} ${RELEASE_FOLDER} folder`);
    } catch (error) {
      console.error(
        `Error while copying ${foler_name} ${RELEASE_FOLDER}`,
        error
      );
    }
  }
};

main();
