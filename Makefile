.PHONY: compile_applications copy_released_apps
DOCKER_IMAGE_NAME=master_web_server


start: clear install_visualizer compile_applications copy_released_apps create_index_html_file

start_remote: clear install_visualizer compile_applications copy_released_apps create_index_html_file build_selenium_image build_nginx_image start_compose 

nginx: build_nginx_image start_nginx_docker

compile_applications:
	node ./helpers/compile_all_applications.js

copy_released_apps:
	node ./helpers/copy_released_apps.js

create_index_html_file:
	node ./helpers/create_index_html_file.js

build_nginx_image:
	docker build -t ${DOCKER_IMAGE_NAME} .

build_selenium_image:
	make -C ./selenium create_docker_image 

start_compose: 
	docker-compose up --build

start_nginx_docker:
	docker run -p 8085:8080 ${DOCKER_IMAGE_NAME}

quick_start: build_selenium_image start_compose

local_selenium: 
	./selenium/workdir/chromedriver --port=4444

install_visualizer:
	npm install --prefix ./visualizer/
 
run_local_visualizer:
	npm run start --prefix ./visualizer/

manually_run_benchmark: clear_results
	HUB_HOST=localhost HUB_PORT=4444 LOCAL=false node selenium/src/index.js

manually_run_benchmark_debug: clear_results
	HUB_HOST=localhost HUB_PORT=4444 LOCAL=false node inspect selenium/src/index.js

manually_run_benchmark_local: clear_results
	HUB_HOST=localhost HUB_PORT=4444 LOCAL=true node selenium/src/index.js

manually_run_benchmark_local_debug: clear_results
	HUB_HOST=localhost HUB_PORT=4444 LOCAL=true node inspect selenium/src/index.js

manually_run_analyser_local: clear_dist
	LOCAL=true node analyser/src/index.js

manually_run_analyser_local_debug: clear_dist
	LOCAL=true node inspect analyser/src/index.js

manually_bench_and_analyse_local: manually_run_benchmark_local manually_run_analyser_local

clear: clear_results clear_dist
	sudo rm -fr ./releases
	sudo rm -fr ./apps/**/dist

clear_results:
	sudo rm -fr ./results

clear_dist:
	sudo rm -fr ./dist