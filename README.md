# Prerequisites:
> https://docs.docker.com/get-docker/

> https://docs.docker.com/compose/install/

> https://nodejs.org/en/download/

# Remote version  ( full benchmark )
## To run dockerized version

```bash
make start_remote
make manually_run_analyser_local
make run_local_visualizer
```

After it, select files from **dist** folder.


# Local version ( developement and debugging )
## To start full local instance:
```bash
make local_selenium
make start_nginx_docker
```

Then to run benchmark:
```bash
make manually_run_benchmark_local
```

Or to in debug mode
```bash
make manually_run_benchmark_local_debug
```
