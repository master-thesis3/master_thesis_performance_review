const { HUB_HOST, HUB_PORT } = process.env;

class Config {
  data = {
    isLocal: false,
    selenium: {
      host: null,
      port: null
    },
    webserver: {
      host: null,
      port: null
    }
  };
  initialize = ({ isLocal }) => {
    this.data.isLocal = isLocal;
    this.data.selenium.host = isLocal ? "localhost" : HUB_HOST;
    this.data.selenium.port = HUB_PORT;

    this.data.webserver.host = isLocal ? "localhost" : "master_web_server";
    this.data.webserver.port = isLocal ? 8085 : 8080;
  };

  getSeleniumUrl = () => {
    const {
      isLocal,
      selenium: { host, port }
    } = this.data;
    return isLocal
      ? `http://${host}:${port}/`
      : `http://${host}:${port}/wd/hub`;
  };

  getSeleniumStatusUrl = () => {
    const {
      selenium: { host, port }
    } = this.data;
    return `http://${host}:${port}/status`;
  };

  getWebServerURL = () => {
    const { host, port } = this.data.webserver;
    return `http://${host}:${port}/`;
  };
}

module.exports = new Config();
