const emoji = require("node-emoji");
const { start_benchmark } = require("./benchmark");
const { create_driver } = require("./driver_support");
const { check_if_server_is_ready, debug_delay } = require("./utils");
const Config = require("./config");
let driver = null;

const { LOCAL = "false" } = process.env;
process.once("SIGINT", async () => {
  console.log(`${emoji.get("skull")} Killing driver`);
  if (!driver) {
    return;
  }
  await driver.quit();
});

const wait_for_server = async (name, path) => {
  console.log(`${emoji.get("hourglass")} Waiting for ${name}`);
  try {
    await check_if_server_is_ready(path);
    console.log(
      `${emoji.get("white_check_mark")} Connected successfully to ${name}!`
    );
  } catch (error) {
    console.error(
      `${emoji.get("no_entry")} Error while connecting to ${name}!`,
      error
    );
    throw Error(error);
  }
};

(async () => {
  Config.initialize({
    isLocal: LOCAL == "true"
  });
  await Promise.all([
    debug_delay(2000), // wait for chrome
    wait_for_server("Web Server", Config.getWebServerURL()),
    wait_for_server("Selenium Server", Config.getSeleniumStatusUrl())
  ]);
  try {
    driver = await create_driver();
    await start_benchmark(driver);
  } catch (error) {
    console.error(error);
  } finally {
    if (!driver) {
      return;
    }
    console.log(`${emoji.get("skull")} Killing driver`);
    await driver.quit();
  }
})();

process.on("uncaughtException", function(err) {
  console.log(err);
});
