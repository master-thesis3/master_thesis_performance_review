const emoji = require("node-emoji");
const { logging, By } = require("selenium-webdriver");
const { create_result_directory, saveToDisk } = require("./utils");
const benchmarks = require("./benchmarks");
const Config = require("./config");

const start_benchmark = async driver => {
  const list_of_links = await get_list_of_links(driver);
  console.log(`${emoji.get(":sunglasses:")} Found: `, list_of_links);

  for (let index = 0; index < list_of_links.length; index++) {
    const application = list_of_links[index];
    try {
      await clear_browser_memory(driver);
      await bench_single_app(driver, application);
    } catch (error) {
      console.error(
        `${emoji.get("no_entry")} Failed to benchmark ${application}`,
        error
      );
    }
  }
  console.log(`${emoji.get("sparkling_heart")} Finished all benchmarks!`);
};

const clear_browser_memory = async driver =>
  await driver.executeScript("window.gc();");

const get_list_of_links = async driver => {
  console.log(`${emoji.get("mag")} Get list of links`);
  await driver.get(Config.getWebServerURL());
  const list_of_links_element = await driver.findElement(
    By.id("list-of-links")
  );
  const list_of_links = await list_of_links_element.getText();
  return list_of_links.split("\n");
};

const bench_single_app = async (driver, application) => {
  const result_folder = `./results/${application}`;
  console.log(`${emoji.get(":rocket:")} Start Benchmark for ${application}`);
  await driver.get(`${Config.getWebServerURL()}/${application}/`);
  for (const [name, benchmark, iterations] of benchmarks) {
    for (let index = 0; index < iterations; index++) {
      const benchmark_result_folder = `${result_folder}/${name}/${index}`;
      await create_result_directory(benchmark_result_folder);
      await clear_browser_memory(driver);
      await benchmark(driver);
      await saveResults(driver, benchmark_result_folder);
      await driver.executeScript(`console.clear()`);
      await driver.executeScript(`location.reload();`);
    }
  }
  console.log(
    `${emoji.get("ok_hand")} Finished benchmark successfully for ${application}`
  );
};

const saveResults = async (driver, result_foler) => {
  console.log("result_foler", result_foler);
  await driver.executeScript(
    `performance.getEntries().forEach(item => console.info(JSON.stringify(item)))`
  );
  await saveToDisk(
    `${result_foler}/result_performance.json`,
    await driver
      .manage()
      .logs()
      .get(logging.Type.PERFORMANCE)
  );
  await saveToDisk(
    `${result_foler}/result_browser.json`,
    await driver
      .manage()
      .logs()
      .get(logging.Type.BROWSER)
  );
  await saveToDisk(
    `${result_foler}/result_driver.json`,
    await driver
      .manage()
      .logs()
      .get(logging.Type.DRIVER)
  );
};

module.exports = {
  start_benchmark
};
