const { save_screenshot } = require('./utils');

const take_and_save_screenshot = async (path) => {
    await save_screenshot(path, await driver.takeScreenshot())
}

module.exports = {
    take_and_save_screenshot,
}