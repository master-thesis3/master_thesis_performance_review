const simple_benchmark = require("./simple_benchmark");
const single_action = require("./single_action");

module.exports = [
  ["single_action", single_action, 20],
  ["simple_benchmark", simple_benchmark, 1]
];
