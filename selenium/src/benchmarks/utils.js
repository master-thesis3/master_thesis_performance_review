const { By } = require("selenium-webdriver");
const assert = require("assert");

const BUTTON_IDS = {
  REPLACE_ALL_ROWS: "replace-all-rows",
  UPDATE_N_ROWS: "update-n-rows",
  SWAP_TWO_ROWS: "swap-two-rows",
  REMOVE_ROW: "remove-row",
  APPEND_ROWS_BEGINNING: "append-rows-start",
  APPEND_ROWS_END: "append-rows-end",
  CLEAR_ROWS: "clear-rows"
};

const replaceAllRows = async ({ driver, no_log }) => {
  const replaceAllRowsButton = await driver.findElement(
    By.id(BUTTON_IDS.REPLACE_ALL_ROWS)
  );
  if (no_log) {
    await replaceAllRowsButton.click();
    return;
  }
  const timestamp = Date.now();
  const start_name = `start_replace_all_click-${timestamp}`;
  const end_name = `end_replace_all_click-${timestamp}`;
  const measure_name = `measure_replace_all_click-${timestamp}`;

  await driver.executeScript(`performance.mark('${start_name}');`);
  await replaceAllRowsButton.click();
  await driver.executeScript(`performance.mark('${end_name}');`);
  await driver.executeScript(
    `performance.measure('${measure_name}}','${start_name}', '${end_name}');`
  );
};

const updateNRows = async ({ driver, no_log }, quantity) => {
  const updateNRowsButton = await driver.findElement(
    By.id(BUTTON_IDS.UPDATE_N_ROWS)
  );
  if (no_log) {
    await updateNRowsButton.click();
    return;
  }

  const timestamp = Date.now();
  const start_name = `start_updateNRows_click-${timestamp}`;
  const end_name = `end_updateNRows_click-${timestamp}`;
  const measure_name = `measure_updateNRows_click-${timestamp}`;

  await driver.executeScript(`performance.mark('${start_name}');`);
  await updateNRowsButton.click();
  await driver.executeScript(`performance.mark('${end_name}');`);
  await driver.executeScript(
    `performance.measure('${measure_name}}','${start_name}', '${end_name}');`
  );
};

const swapTwoRows = async ({ driver, no_log }, index_a, index_b) => {
  const swapTwoRowsButton = await driver.findElement(
    By.id(BUTTON_IDS.SWAP_TWO_ROWS)
  );
  if (no_log) {
    await swapTwoRowsButton.click();
    return;
  }

  const timestamp = Date.now();
  const start_name = `start_swapTwoRows_click-${timestamp}`;
  const end_name = `end_swapTwoRows_click-${timestamp}`;
  const measure_name = `measure_swapTwoRows_click-${timestamp}`;

  await driver.executeScript(`performance.mark('${start_name}');`);
  await swapTwoRowsButton.click();
  await driver.executeScript(`performance.mark('${end_name}');`);
  await driver.executeScript(
    `performance.measure('${measure_name}}','${start_name}', '${end_name}');`
  );
};

const removeRow = async ({ driver, no_log }, index) => {
  const removeRowButton = await driver.findElement(
    By.id(BUTTON_IDS.REMOVE_ROW)
  );
  if (no_log) {
    await removeRowButton.click();
    return;
  }

  const timestamp = Date.now();
  const start_name = `start_removeRow_click-${timestamp}`;
  const end_name = `end_removeRow_click-${timestamp}`;
  const measure_name = `measure_removeRow_click-${timestamp}`;

  await driver.executeScript(`performance.mark('${start_name}');`);
  await removeRowButton.click();
  await driver.executeScript(`performance.mark('${end_name}');`);
  await driver.executeScript(
    `performance.measure('${measure_name}}','${start_name}', '${end_name}');`
  );
};

const appendNRowsBeginning = async ({ driver, no_log }, quantity) => {
  const appendNRowsBeginningButton = await driver.findElement(
    By.id(BUTTON_IDS.APPEND_ROWS_END)
  );
  if (no_log) {
    await appendNRowsBeginningButton.click();
    return;
  }

  const timestamp = Date.now();
  const start_name = `start_appendNRowsBeginning_click-${timestamp}`;
  const end_name = `end_appendNRowsBeginning_click-${timestamp}`;
  const measure_name = `measure_appendNRowsBeginning_click-${timestamp}`;

  await driver.executeScript(`performance.mark('${start_name}');`);
  await appendNRowsBeginningButton.click();
  await driver.executeScript(`performance.mark('${end_name}');`);
  await driver.executeScript(
    `performance.measure('${measure_name}}','${start_name}', '${end_name}');`
  );
};

const appendNRowsEnd = async ({ driver, no_log }, quantity) => {
  const appendNRowsEndButton = await driver.findElement(
    By.id(BUTTON_IDS.APPEND_ROWS_END)
  );
  if (no_log) {
    await appendNRowsEndButton.click();
    return;
  }
  const timestamp = Date.now();
  const start_name = `start_appendNRowsEnd_click-${timestamp}`;
  const end_name = `end_appendNRowsEnd_click-${timestamp}`;
  const measure_name = `measure_appendNRowsEnd_click-${timestamp}`;

  await driver.executeScript(`performance.mark('${start_name}');`);
  await appendNRowsEndButton.click();
  await driver.executeScript(`performance.mark('${end_name}');`);
  await driver.executeScript(
    `performance.measure('${measure_name}}','${start_name}', '${end_name}');`
  );
};

const clearRows = async ({ driver, no_log }) => {
  const clearRowsButton = await driver.findElement(
    By.id(BUTTON_IDS.CLEAR_ROWS)
  );
  if (no_log) {
    await clearRowsButton.click();
    return;
  }

  const timestamp = Date.now();
  const start_name = `start_clearRows_click-${timestamp}`;
  const end_name = `end_clearRows_click-${timestamp}`;
  const measure_name = `measure_clearRows_click-${timestamp}`;

  await driver.executeScript(`performance.mark('${start_name}');`);
  await clearRowsButton.click();
  await driver.executeScript(`performance.mark('${end_name}');`);
  await driver.executeScript(
    `performance.measure('${measure_name}}','${start_name}', '${end_name}');`
  );
};

module.exports = {
  replaceAllRows,
  updateNRows,
  swapTwoRows,
  removeRow,
  appendNRowsBeginning,
  appendNRowsEnd,
  clearRows
};
