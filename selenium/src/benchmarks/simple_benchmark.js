const assert = require("assert");
const {
  replaceAllRows,
  updateNRows,
  swapTwoRows,
  removeRow,
  appendNRowsBeginning,
  appendNRowsEnd,
  clearRows
} = require("./utils");

module.exports = async driver => {
  assert.ok(!!driver, "Driver not passed to benchmark!");
  await replaceAllRows({driver});
  await updateNRows({driver}, 1000);
  await swapTwoRows({driver}, 0, 1);
  await removeRow({driver}, 1);
  await appendNRowsBeginning({driver}, 1000);
  await appendNRowsEnd({driver}, 1000);
  await clearRows({driver});
};
