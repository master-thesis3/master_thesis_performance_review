const assert = require("assert");

const {
  replaceAllRows,
  updateNRows,
  swapTwoRows,
  removeRow,
  appendNRowsBeginning,
  appendNRowsEnd,
  clearRows
} = require("./utils");

const REPEAT_NUMBER = 100;

const runBenchmarkNTimes = async (benchmark, ...args) => {
  for (let index = 0; index < REPEAT_NUMBER; index++) {
    await clearRows({ driver, no_log: true });
    await appendNRowsBeginning({ driver, no_log: true }, 1000);
    await benchmark({ driver }, ...args);
  }
};

module.exports = async driver => {
  assert.ok(!!driver, "Driver not passed to benchmark!");
  await runBenchmarkNTimes(appendNRowsBeginning, 1000);
  await runBenchmarkNTimes(appendNRowsEnd, 1000);
  await runBenchmarkNTimes(replaceAllRows);
  await runBenchmarkNTimes(updateNRows, 500);
  await runBenchmarkNTimes(swapTwoRows, 0, 1);
  await runBenchmarkNTimes(removeRow, 1);
  await runBenchmarkNTimes(clearRows);
};
