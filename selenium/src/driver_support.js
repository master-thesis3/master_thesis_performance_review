const emoji = require("node-emoji");
const path = require("path");
const { Builder, logging } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome");
const Config = require("./config");
const DRIVER_PATH = path.resolve(
  `${__dirname}/../workdir/chromedriver_linux64.zip`
);

const create_driver = async () => {
  console.log(`${emoji.get(":computer:")} Creating driver...`);
  try {
    chrome.setDefaultService(new chrome.ServiceBuilder(DRIVER_PATH).build());
    const chromeOptions = new chrome.Options();
    let logPref = new logging.Preferences();
    logPref.setLevel(logging.Type.PERFORMANCE, logging.Level.ALL);
    logPref.setLevel(logging.Type.BROWSER, logging.Level.ALL);
    logPref.setLevel(logging.Type.DRIVER, logging.Level.ALL);
    chromeOptions.setLoggingPrefs(logPref);
    chromeOptions.addArguments("--js-flags=--expose-gc");
    chromeOptions.addArguments("--disable-extensions");
    chromeOptions.addArguments("--enable-gpu-benchmarking");
    // chromeOptions.setPerfLoggingPrefs({
    //   traceCategories: [
    //     // ...["-*", "devtools.timeline", "disabled-by-default-devtools.timeline", "disabled-by-default-devtools.timeline.frame", "toplevel", "blink.console", "disabled-by-default-devtools.timeline.stack", "disabled-by-default-devtools.screenshot", "disabled-by-default-v8.cpu_profile", "disabled-by-default-v8.cpu_profiler", "disabled-by-default-v8.cpu_profiler.hires"],
    //     "v8",
    //     "blink.console,browser",
    //     "disabled-by-default-devtools.timeline,devtools.timeline",
    //     "devtools",
    //     "disabled-by-default-devtools.timeline.frame",
    //     "disabled-by-default-devtools.screenshot",
    //     "toplevel",
    //     "benchmark",
    //     "latencyInfo",
    //     "rail",
    //     "input"
    //   ].join(",")
    // });
    chromeOptions.headless();

    if (!Config.data.isLocal) {
      chromeOptions.headless();
    }
    driver = await new Builder()
      .forBrowser("chrome")
      .setChromeOptions(chromeOptions)
      .usingServer(Config.getSeleniumUrl())
      .build();
    console.log(`${emoji.get("star")} Driver created`);
    return driver;
  } catch (error) {
    console.error("Error while creating driver");
    throw new Error(error);
  }
};

module.exports = {
  create_driver
};
