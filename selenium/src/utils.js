const fs = require("fs");
const RETRY = 3;
const DELAY = 1000;
const axios = require("axios").default;

const check_if_server_is_ready = async url => {
  for (let index = 0; index < RETRY; index++) {
    try {
      await connect_to_page(url);
      return true;
    } catch (error) {
      console.error("Fail nr", `${index + 1}/${RETRY}`, error);
      await debug_delay(DELAY);
    }
  }
  throw new Error("Can't connect to the server");
};

const connect_to_page = async url => {
  try {
    await axios.get(url);
  } catch (error) {
    throw new Error(error)
  }
};

const save_screenshot = async (path, data) => {
  await fs.promises.writeFile(path, data, "base64");
};

const create_result_directory = async path => {
  await fs.promises.mkdir(path, { recursive: true });
};

const saveToDisk = async (path, data) => {
  await fs.promises.writeFile(path, JSON.stringify(data, null, 2));
};

const debug_delay = timeout =>
  new Promise(resolve => {
    setTimeout(() => resolve(), timeout);
  });

module.exports = {
  check_if_server_is_ready,
  save_screenshot,
  create_result_directory,
  saveToDisk,
  debug_delay
};
